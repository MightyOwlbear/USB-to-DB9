# USB-to-DB9

Mirror of (lightly modified) code required for USB to DB9 joystick converter

PicoGamepad library by [RealRobots](https://gitlab.com/realrobots/PicoGamepad)

DB9 joystick INO file based on [NickZero's work](https://www.instructables.com/Retro-9-Pin-Joystick-to-USB-Converter/)
