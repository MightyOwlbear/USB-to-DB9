/*
  Pico Joystick Converter

  Original Code by
  by Jake Wilkinson (RealRobots)
  https://www.gitlab.com/realrobots/PicoGamepad

  Modified by Nick Stevens Feb 2022

*/

#include <PicoGamepad.h>

#define PIN_BTN_0 6
#define PIN_BTN_1 2
#define PIN_BTN_2 3
#define PIN_BTN_3 4
#define PIN_BTN_4 5

PicoGamepad gamepad;

// 16 bit integer for holding input values
int val;

void setup() {
  Serial.begin(115200);

  pinMode(LED_BUILTIN, OUTPUT);

  // Button on pin
  pinMode(PIN_BTN_0, INPUT_PULLUP);
  pinMode(PIN_BTN_1, INPUT_PULLUP);
  pinMode(PIN_BTN_2, INPUT_PULLUP);
  pinMode(PIN_BTN_3, INPUT_PULLUP);
  pinMode(PIN_BTN_4, INPUT_PULLUP);
}

void loop() {

  // Fire Button & flash the LED when pressed
  if (!digitalRead(PIN_BTN_0)) {
    gamepad.SetButton(0, 1);
    digitalWrite(LED_BUILTIN, 1);
  } else {
    gamepad.SetButton(0, 0);
    digitalWrite(LED_BUILTIN, 0);
  }

  // Left, Right, Up, Down - uncomment these to map to buttons 1 -4
  // gamepad.SetButton(1, !digitalRead(PIN_BTN_1));
  // gamepad.SetButton(2, !digitalRead(PIN_BTN_2));
  // gamepad.SetButton(3, !digitalRead(PIN_BTN_3));
  // gamepad.SetButton(4, !digitalRead(PIN_BTN_4));

  // Set hat direction, 4 hats available. direction is clockwise 0=N 1=NE 2=E 3=SE 4=S 5=SW 6=W 7=NW 8=CENTER
  int hat0_dir = 8;

  // Centre the X and Y Axis
  gamepad.SetX(0);
  gamepad.SetY(0);

  //Check where joystick is pointed and set values accordingly
  if (!digitalRead(PIN_BTN_1)) {
    // North
    hat0_dir = 0;
    gamepad.SetY(-32767);
    gamepad.SetX(0);
  }
  if (!digitalRead(PIN_BTN_2)) {
    // South
    hat0_dir = 4;
    gamepad.SetY(32767);
    gamepad.SetX(0);
  }
  if (!digitalRead(PIN_BTN_3)) {
    // East
    hat0_dir = 6;
    gamepad.SetX(-32767);
    gamepad.SetY(0);
  }
  if (!digitalRead(PIN_BTN_4)) {
    // West
    hat0_dir = 2;
    gamepad.SetX(32767);
    gamepad.SetY(0);
  }
  if (!digitalRead(PIN_BTN_1) && !digitalRead(PIN_BTN_4)) {
    // NE
    hat0_dir = 1;
    gamepad.SetY(-32767);
    gamepad.SetX(32767);
  }
  if (!digitalRead(PIN_BTN_4) && !digitalRead(PIN_BTN_2)) {
    // SE
    hat0_dir = 3;
    gamepad.SetY(32767);
    gamepad.SetX(32767);
  }
  if (!digitalRead(PIN_BTN_3) && !digitalRead(PIN_BTN_2)) {
    // SW
    hat0_dir = 5;
    gamepad.SetY(32767);
    gamepad.SetX(-32767);
  }
  if (!digitalRead(PIN_BTN_3) && !digitalRead(PIN_BTN_1)) {
    // NW
    hat0_dir = 7;
    gamepad.SetY(-32767);
    gamepad.SetX(-32767);
  }

  //Set HAT direction - uncomment if you want to use the HAT
  //gamepad.SetHat(0, hat0_dir);


  // Send all inputs via HID
  // Nothing is send to your computer until this is called.
  gamepad.send_update();
}
